#pragma once
#include <io.h>
#include <vector>
#include <sstream>
#include <map>
#include <algorithm>
#include <deque>

#include "IDataAccess.h"
#include "sqlite3.h"
#include "SqlException.h"

#define PATH "../galleryDB.sqlite"
#define NOT_FOUND false
#define FOUND true
#define PASS true

enum table_names
{
	USERS,
	ALBUMS,
	PICTURES,
	TAGS
};

enum user_values
{
	USER_ID,
	USER_NAME
};

enum picture_values
{
	PIC_ID,
	PIC_NAME,
	PIC_LOCATION,
	PIC_CREATION_DATE,
	PIC_ALBUM_ID
};

enum album_values
{
	ALBUM_ID,
	ALBUM_NAME,
	ALBUM_USER_ID,
	ALBUM_CREATION_DATE
};

using std::string;

class DatabaseAccess : public IDataAccess
{
public:
	DatabaseAccess();
	~DatabaseAccess();

	// album related
	const std::list<Album> getAlbums();
	const std::list<Album> getAlbumsOfUser(const User& user);
	void createAlbum(const Album& album);
	void deleteAlbum(const std::string& albumName, int userId);
	bool doesAlbumExists(const std::string& albumName, int userId);
	Album openAlbum(const std::string& albumName);
	void closeAlbum(Album& pAlbum);
	void printAlbums();

	// picture related
	void addPictureToAlbumByName(const std::string& albumName, const Picture& picture);
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName);
	void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId);
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId);

	// user related
	void printUsers();
	User getUser(int userId);
	void createUser(User& user);
	void deleteUser(const User& user);
	bool doesUserExists(int userId);


	// user statistics
	int countAlbumsOwnedOfUser(const User& user);
	int countAlbumsTaggedOfUser(const User& user);
	int countTagsOfUser(const User& user);
	float averageTagsPerAlbumOfUser(const User& user);

	// queries
	User getTopTaggedUser();
	Picture getTopTaggedPicture();
	std::list<Picture> getTaggedPicturesOfUser(const User& user);

	bool open();
	void close();
	void clear();
	void clear_tables();


	// callbacks
	friend int callback(void *data, int argc, char **argv, char **azColName);
	friend int printCallback(void *data, int argc, char **argv, char **azColName);
	friend int albumCallback(void *data, int argc, char **argv, char **azColName);
	friend int counterCallback(void *data, int argc, char **argv, char **azColName);
	friend int userCallback(void *data, int argc, char **argv, char **azColName);
	friend int pictureCallback(void *data, int argc, char **argv, char **azColName);
	friend int tagCallback(void *data, int argc, char **argv, char **azColName);
	friend int avgTagCallback(void *data, int argc, char **argv, char **azColName);


private:
	sqlite3* _database;
	std::vector<string> _tables;
	std::list<Album> _saveAlbums;
	std::list<Picture> _savePictures;
	std::map<string, string> _selectData;
	std::set<int> _taggedAlbums;
	int _countAlbums;
	int _userTagged;
	string _searchKey;
	bool _found;
	User _saveUser;
	int _counter;

	// SQL helper
	bool init();
	int execute(std::string str_exe, int(*callback)(void*, int, char**, char**) = nullptr, void* data = nullptr);
	string find_id_album(string name);
	string find_id_picture(string name, string album_name);
	static string add_quotations(std::deque<string> str);
	void removeAllPicturesAlbum(string album_id);
	std::list<Picture> getPicturesOfAlbum(int album_id);
	Picture getPictureById(int id);
};