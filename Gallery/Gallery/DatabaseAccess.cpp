#include "DatabaseAccess.h"

DatabaseAccess::DatabaseAccess() : IDataAccess()
{
	// For the deletion of the tables.
	this->_tables.push_back("Users");
	this->_tables.push_back("Albums");
	this->_tables.push_back("Pictures");
	this->_tables.push_back("Tags");
	
	// Openning the database.
	this->open();
}


DatabaseAccess::~DatabaseAccess()
{
	this->close();
}


bool DatabaseAccess::open()
{
	bool result = PASS;
	int fileExists = _access(PATH, 0); // File status => 0 if exists, 1 if it doesn't.
	// For next when we are adding the tables.

	int ans = sqlite3_open(PATH, &this->_database);
	if (ans != SQLITE_OK)
	{
		throw OpenException();
	}

	if (fileExists) // Goes in if file didn't exist.
	{
		// Init database
		result = this->init();
	}
	return result;
}

bool DatabaseAccess::init()
{ // Setting of the tables.
	int ans = SQLITE_OK;
	char** errMsg = nullptr;

	string users = R"(CREATE TABLE IF NOT EXISTS Users(
							ID INTEGER PRIMARY KEY AUTOINCREMENT, 
							Name TEXT NOT NULL
							);)";

	string albums = R"(CREATE TABLE IF NOT EXISTS Albums(
					   ID INTEGER PRIMARY KEY AUTOINCREMENT,
					   NAME TEXT NOT NULL,
					   CREATION_DATE TEXT,
					   USER_ID INTEGER NOT NULL,
					   FOREIGN KEY (USER_ID) REFERENCES Users(ID) ON DELETE CASCADE
					   );)";

	string pictures = R"(CREATE TABLE IF NOT EXISTS Pictures(
						ID INTEGER PRIMARY KEY AUTOINCREMENT,
						NAME TEXT NOT NULL,
						LOCATION TEXT NOT NULL,
						CREATION_DATE TEXT,
						ALBUM_ID TEXT NOT NULL,
						FOREIGN KEY (ALBUM_ID) REFERENCES Albums(ID) ON DELETE CASCADE
						);)";

	string tags = R"(CREATE TABLE IF NOT EXISTS Tags(
					ID INTEGER PRIMARY KEY AUTOINCREMENT,
					PICTURE_ID INTEGER NOT NULL,
					USER_ID INTEGER NOT NULL,
					FOREIGN KEY (PICTURE_ID) REFERENCES Pictures(ID) ON DELETE CASCADE,
					FOREIGN KEY (USER_ID) REFERENCES Users(ID) ON DELETE CASCADE
					);)";

	// init of all tables.
	ans += this->execute(users);
	ans += this->execute(albums);
	ans += this->execute(pictures);
	ans += this->execute(tags);

	if (ans != SQLITE_OK)
	{ // if all went right then ans variable wouldn't get any big, but stay the same.
		throw InitException();
	}
	return PASS;
}

void DatabaseAccess::close()
{
	int ans = sqlite3_close(this->_database);
	if (ans != SQLITE_OK)
	{
		throw CloseException();
	}

	this->_database = nullptr;
}

/*********
SQL Helper
*********/

void DatabaseAccess::clear_tables()
{// Deleting all info from tables
	int ans = SQLITE_OK;

	for (auto it = this->_tables.begin(); it != this->_tables.end(); ++it)
	{
		string insert_str = "DELETE FROM " + *it + ";"; // All the rows.

		ans = this->execute(insert_str);
		if (ans != SQLITE_OK)
		{
			throw DeleteException();
		}
	}
}

int DatabaseAccess::execute(std::string str_exe, int(*callback)(void*, int, char**, char**), void* data)
{ // For fast use.
	int ans = SQLITE_OK;
	char** errMsg = nullptr;

	ans = sqlite3_exec(this->_database, str_exe.c_str(), callback, data, errMsg);
	return ans;
}

int callback(void *data, int argc, char **argv, char **azColName)
{ // called when ever theres a select that passed the conditions.
	DatabaseAccess* ths = (DatabaseAccess*)data;
	std::map<string, string> save;

	for (int i = 0; i < argc; i++)
	{
		string column = azColName[i];
		string value = argv[i];

		if (ths->_searchKey == value)
		{
			ths->_found = true;
		}
		save.insert(std::pair<string, string>(column, value));
	}

	if (!ths->_found)
	{// If the found row isn't the one we wanted then we won't add to map.
		throw NotFoundException();
	}
	ths->_selectData = save;

	return 0;
}

int printCallback(void *data, int argc, char **argv, char **azColName)
{
	for (int i = 0; i < argc; i++)
	{ // print all values of row.
		std::cout << azColName[i] << " = " << argv[i] << " , ";
	}
	std::cout << std::endl;

	return 0;
}

int albumCallback(void *data, int argc, char **argv, char **azColName)
{
	DatabaseAccess* ths = (DatabaseAccess*)data;

	Album save(std::stoi(argv[ALBUM_USER_ID]), argv[ALBUM_NAME], argv[ALBUM_CREATION_DATE]); // user_id, name, creation_date.

	ths->getPicturesOfAlbum(std::stoi(argv[ALBUM_ID]));
	for (auto i : ths->_savePictures)
	{ // adding all the pictures to album.
		save.addPicture(i);
	}

	ths->_saveAlbums.push_back(save);
	ths->_found = true;

	return 0;
}

int counterCallback(void *data, int argc, char **argv, char **azColName)
{
	DatabaseAccess* ths = (DatabaseAccess*)data;
	ths->_counter += 1; // adding 1 to counter of objects.
	return 0;
}

int userCallback(void *data, int argc, char **argv, char **azColName)
{
	DatabaseAccess* ths = (DatabaseAccess*)data;
	ths->_saveUser = User(std::stoi(argv[USER_ID]), argv[USER_NAME]); // ID, name.
	ths->_found = true;

	return 0;
}

int pictureCallback(void *data, int argc, char **argv, char **azColName)
{
	DatabaseAccess* ths = (DatabaseAccess*)data;
	
	Picture save(std::stoi(argv[PIC_ID]), argv[PIC_NAME], argv[PIC_LOCATION], argv[PIC_CREATION_DATE]); // ID, name, Path, Creation_date.
	ths->_savePictures.push_back(save);

	return 0;
}

int tagCallback(void *data, int argc, char **argv, char **azColName)
{
	DatabaseAccess* ths = (DatabaseAccess*)data;

	if (std::stoi(argv[USER_ID]) == ths->_userTagged)
	{
		ths->_taggedAlbums.insert(std::stoi(argv[1])); // In the select he is second, so 1.
	}

	return 0;
}

int avgTagCallback(void *data, int argc, char **argv, char **azColName)
{
	DatabaseAccess* ths = (DatabaseAccess*)data;
	
	if (std::stoi(argv[1]) == ths->_userTagged)
	{
		ths->_countAlbums += 1;
	}

	return 0;
}

string DatabaseAccess::add_quotations(std::deque<string> str)
{
	string save = "";
	while (!str.empty())
	{
		save += "'" + str.front() + "',";
		str.pop_front();
	}
	save.pop_back();
	return save;
}

/************
Album Related
************/
void DatabaseAccess::createAlbum(const Album& album)
{
	int ans;
	std::deque<string> values;
	std::stringstream strStream;
	std::list<Picture> pic_list = album.getPictures();

	values.push_back(album.getName());
	values.push_back(std::to_string(album.getOwnerId()));
	values.push_back(album.getCreationDate());

	strStream << "INSERT INTO Albums\n"
		<< "(name, user_id, creation_date)\n"
		<< "VALUES (" << this->add_quotations(values) << ");";
	string str = strStream.str();

	ans = this->execute(str);
	if (ans != SQLITE_OK)
	{
		throw InsertException();
	}

	for (auto const& i : pic_list)
	{
		this->addPictureToAlbumByName(album.getName(), i);
	}
}

Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	int ans;
	this->_saveAlbums.clear();
	this->_found = false;

	string str = "SELECT * FROM Albums\
					WHERE name LIKE '" + albumName + "';";

	ans = this->execute(str, albumCallback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	if (!this->_found)
	{// album not found! if we didn't find the wanted data.
		throw NotFoundException();
	}

	Album save(this->_saveAlbums.front());

	return save;
}

std::list<Picture> DatabaseAccess::getPicturesOfAlbum(int album_id)
{
	int ans;
	this->_savePictures.clear();

	string str = "SELECT * FROM Pictures \
					WHERE album_id = " + std::to_string(album_id) + ";";

	ans = this->execute(str, pictureCallback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	return this->_savePictures;
}

void DatabaseAccess::clear()
{
	this->clear_tables();
}

const std::list<Album> DatabaseAccess::getAlbums()
{
	int ans;
	this->_saveAlbums.clear();
	string str = "SELECT * FROM Albums;";

	ans = this->execute(str, albumCallback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	return this->_saveAlbums;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	int ans;
	this->_saveAlbums.clear();

	string str = "SELECT * FROM Albums \
					WHERE user_id = " + std::to_string(user.getId()) + ";";

	ans = this->execute(str, albumCallback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	return this->_saveAlbums;
}

void DatabaseAccess::closeAlbum(Album& pAlbum)
{
	// basically here we would like to delete the allocated memory we got from openAlbum
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{ // removing an album from the table.
	int ans;
	std::stringstream strStream;
	string str_id = this->find_id_album(albumName);

	// No need, ON DELETE CASCADE.
	this->removeAllPicturesAlbum(str_id); // But only when I am using the made table.
	strStream << "DELETE FROM Albums\n"
			<< "WHERE name LIKE '" << albumName << "'\n"
			<< "AND user_id = " << userId << ";";
	string str = strStream.str();

	ans = this->execute(str);
	if (ans != SQLITE_OK)
	{
		throw DeleteException();
	}
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	int ans;
	this->_saveAlbums.clear();
	this->_found = false;

	string str = "SELECT * FROM Albums\
					WHERE name LIKE '" + albumName + "'\
					AND User_id = " + std::to_string(userId) + ";";

	ans = this->execute(str, albumCallback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}

	return this->_found;
}

string DatabaseAccess::find_id_album(string name)
{
	int ans;
	std::stringstream strStream;

	// selecting wanted variable
	strStream << "SELECT * FROM Albums\n"
		<< "WHERE name LIKE '" << name << "';";

	string str = strStream.str();
	this->_searchKey = name;

	ans = this->execute(str, callback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}

	return this->_selectData["ID"];
}

string DatabaseAccess::find_id_picture(string name, string album_name)
{
	int ans;
	std::stringstream strStream;
	string album_id = this->find_id_album(album_name);

	// selecting wanted variable
	strStream << "SELECT * FROM Pictures\n"
		<< "WHERE name LIKE '" << name << "'\n"
		<< "AND album_id = " << album_id << ";";

	string str = strStream.str();
	this->_searchKey = name;

	ans = this->execute(str, callback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}

	return this->_selectData["ID"];
}

void DatabaseAccess::printAlbums()
{
	int ans;
	string str = "SELECT * FROM Albums;";

	ans = this->execute(str, printCallback);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
}

/**************
Picture Related
***************/
void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	int ans;
	std::deque<string> values;
	std::stringstream strStream;
	string str_id = this->find_id_album(albumName);

	values.push_back(picture.getName());
	values.push_back(picture.getPath());
	values.push_back(picture.getCreationDate());
	values.push_back(str_id);

	strStream << "INSERT INTO pictures\n"
		<< "(name, location, creation_date, album_id)\n"
		<< "VALUES (" << this->add_quotations(values) << ");";
	string str = strStream.str();

	ans = this->execute(str);
	if (ans != SQLITE_OK)
	{
		throw InsertException();
	}
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	int ans;
	std::stringstream strStream;
	string str_id = this->find_id_album(albumName);

	strStream << "DELETE FROM pictures\n"
		<< "WHERE name LIKE '"<< pictureName <<"'\n"
		<< "AND album_id = " << str_id << ";";;
	string str = strStream.str();

	ans = this->execute(str);
	if (ans != SQLITE_OK)
	{
		throw DeleteException();
	}
}

void DatabaseAccess::removeAllPicturesAlbum(string album_id)
{
	int ans;
	std::stringstream strStream;

	strStream << "DELETE FROM pictures\n"
		<< "WHERE album_id = " << album_id << ";";
	string str = strStream.str();

	ans = this->execute(str);
	if (ans != SQLITE_OK)
	{
		throw DeleteException();
	}
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	int ans;
	std::stringstream strStream;
	string picture_id = this->find_id_picture(pictureName, albumName); // finding the id of picture.

	strStream << "INSERT INTO tags\n"
		<< "(User_id, Picture_id)\n"
		<< "VALUES (" << userId << "," << picture_id << ");";
	string str = strStream.str();

	ans = this->execute(str);
	if (ans != SQLITE_OK)
	{
		throw InsertException();
	}
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	int ans;
	std::stringstream strStream;
	string picture_id = this->find_id_picture(pictureName, albumName); // finding the id of picture.

	strStream << "DELETE FROM tags\n"
		<< "WHERE picture_id = " << picture_id << "\n"
		<< "AND user_id = " << userId << ";";
	string str = strStream.str();

	ans = this->execute(str);
	if (ans != SQLITE_OK)
	{
		throw DeleteException();
	}
}

Picture DatabaseAccess::getPictureById(int id)
{

	int ans;
	std::stringstream strStream;
	this->_savePictures.clear();

	// selecting wanted variable
	strStream << "SELECT * FROM Pictures\n"
		<< "WHERE id = " << id << ";";
	string str = strStream.str();

	ans = this->execute(str, pictureCallback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}

	return this->_savePictures.front();
}

/***********
User Related
***********/
void DatabaseAccess::createUser(User& user)
{
	int ans;
	std::stringstream strStream;
	string name = user.getName();

	strStream << "INSERT INTO Users\n"
		<< "(name)\n"
		<< "VALUES ('" << name << "');";
	string str = strStream.str();

	ans = this->execute(str);
	if (ans != SQLITE_OK)
	{
		throw InsertException();
	}
}

void DatabaseAccess::deleteUser(const User& user)
{
	int ans;
	std::stringstream strStream;
	string name = user.getName();

	strStream << "DELETE FROM Users\n"
		<< "WHERE name LIKE '" << name << "';";
	string str = strStream.str();

	ans = this->execute(str);
	if (ans != SQLITE_OK)
	{
		throw DeleteException();
	}
}

User DatabaseAccess::getUser(int userId)
{
	int ans;
	std::stringstream strStream;
	this->_found = false;

	// selecting wanted variable
	strStream << "SELECT * FROM Users\n"
		<< "WHERE id = " << userId << ";";

	string str = strStream.str();
	this->_searchKey = std::to_string(userId);

	ans = this->execute(str, userCallback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	if (!this->_found)
	{// if we didn't find the wanted data.
		throw NotFoundException();
	}

	return this->_saveUser;
}

bool DatabaseAccess::doesUserExists(int userId)
{
	int ans;
	std::stringstream strStream;

	// selecting wanted variable
	strStream << "SELECT * FROM Users\n"
		<< "WHERE id = " << userId << ";";

	string str = strStream.str();
	this->_searchKey = std::to_string(userId);

	ans = this->execute(str, callback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	return this->_found;
}

void DatabaseAccess::printUsers()
{
	int ans;
	string str = "SELECT * FROM Users;";

	ans = this->execute(str, printCallback);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
}

/**************
User Statistics
**************/
int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	int ans;
	this->_counter = 0; // reseting the counter to start with nothing.

	string str = "SELECT * FROM Albums \
					WHERE user_id = " + std::to_string(user.getId()) + ";";

	// counting all albums of user.
	ans = this->execute(str, counterCallback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	return this->_counter;
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	int ans;
	this->_counter = 0; // reseting the counter to start with nothing.

	string str = "SELECT * FROM Tags \
					WHERE user_id = " + std::to_string(user.getId()) + ";";

	// counting all tags of user.
	ans = this->execute(str, counterCallback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	return this->_counter;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	int ans;
	this->_countAlbums = 0;
	float num_of_albums = this->countAlbumsOwnedOfUser(user);
	this->_userTagged = user.getId();

	string str = "SELECT tags.user_id, albums.user_id as id, albums.id FROM tags\n \
					JOIN pictures, albums\n\
					ON pictures.id = tags.picture_id AND albums.id = pictures.album_id;";
	ans = this->execute(str, avgTagCallback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	bool no_albums = num_of_albums == 0;

	return no_albums ? 0 : this->_countAlbums / num_of_albums;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	int ans;
	this->_userTagged = user.getId();
	string str = "SELECT tags.user_id, pic.album_id FROM Tags \
					JOIN pictures pic\
					ON pic.id = tags.picture_id;";

	// collecting all tags of user.
	ans = this->execute(str, tagCallback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	// the set has all the albums which the user is tagged in.
	return this->_taggedAlbums.size();
}

/******
Queries
******/
User DatabaseAccess::getTopTaggedUser()
{
	int ans;
	this->_found = true; // so it won't give us an error
	string str = R"(SELECT user_id FROM tags
					GROUP BY user_id
					ORDER BY COUNT(*) DESC
					LIMIT 1;)";
	
	// getting most frequent user_id in tags.
	ans = this->execute(str, callback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	User save = this->getUser(std::stoi(this->_selectData.begin()->second));
	return save;
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	int ans;
	this->_found = true; // so it won't give us an error
	string str = R"(SELECT picture_id FROM tags
					GROUP BY picture_id
					ORDER BY COUNT(*) DESC
					LIMIT 1;)";

	// getting most frequent picture_id in tags.
	ans = this->execute(str, callback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	// getting the top id from the callback and then finding the picture details.
	return this->getPictureById(std::stoi(this->_selectData.begin()->second));
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	int ans;
	this->_found = true; // so it won't give us an error
	this->_userTagged = user.getId();
	std::list<Picture> list_pictures;

	string str = "SELECT user_id, picture_id FROM tags \
					WHERE user_id = " + std::to_string(user.getId()) + ";";

	// getting most frequent picture_id in tags.
	ans = this->execute(str, tagCallback, this);
	if (ans != SQLITE_OK)
	{
		throw AccessException();
	}
	// converting all the IDs of pictures to pictures.
	for (auto i : this->_taggedAlbums)
	{
		list_pictures.push_back(this->getPictureById(i));
	}
	return list_pictures;
}